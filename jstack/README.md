# Jstack
Jstack jest implementacją stosu w Javie.
## Technologie
* Java8 lub nowsza
* JUnit 5
* Junit 4
* pitest - http://pitest.org/
## Uwagi techniczne
### Preferowane IDE
IntelliJ
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE.
### Uruchamianie analizy mutacji oraz pokrycia kodu testami
Moduł stack-quality projektu jstack ma specjalnie przygotowane testy do analizy pokrycia kodu testami oraz do analizy pokrycia mutacji. W module znajdują się dwie klasy testujące: `StackTestWeak` oraz `StackTestNice`, które są przygotowane do uruchamiana z pluginem pitest.

Analizę można uruchomić z konsoli komendą:

`mvn org.pitest:pitest-maven:mutationCoverage`

Lub też z IDE wykorzystując zakładkę maven i wpisując goal: 

`mvn org.pitest:pitest-maven:mutationCoverage`

Przy odpalaniu z IDE należy wskazać stack-quality jako working directory.

Klasę testującą do bieżącej analizy należy podać w pliku stack-quality/pom.xml, szukając fragmentu wedle poniższych wskazówek.

Wybranie klasy do testów ze słabym pokryciem mutacji:
```xml
<configuration>
    <targetTests>
        <param>
            pl.poznan.put.spio.quality.StackTestWeak
        </param>
    </targetTests>
</configuration>
```
Wybranie klasy do testów z silnym pokryciem mutacji:
```xml
<configuration>
    <targetTests>
        <param>
            pl.poznan.put.spio.quality.StackTestNice
        </param>
    </targetTests>
</configuration>
```
Wyniki analizy znajdują się w stack-quality/target/pit-reports.