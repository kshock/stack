# To jest miejsce od którego należy zacząć zadanie TDD
import unittest
from stack.stack import Stack


class unittest_StackTest(unittest.TestCase):
    stack = None

    def setUp(self):
        self.stack = Stack()

    def tearDown(self):
        self.stack = None

    def test_copy(self):
        # given
        self.stack.push('lubie placki')

        # when
        another_stack = self.stack.copy()

        # then
        self.assertEqual(self.stack.stack, another_stack)

    def test_popleft(self):
        # given
        for i in range(12):
            self.stack.push(i)
        first_state = self.stack.stack.copy()
        # when
        self.stack.popleft()
        result = self.stack.stack
        # then
        self.assertEqual(len(first_state)-1, len(result))
        self.assertEqual(first_state[-1], result[-1])

    def test_reverse(self):
        # given
        for i in range(12):
            self.stack.push(i)
        first_state = self.stack.stack.copy()
        # when
        self.stack.reverse()
        result = self.stack.stack
        # then
        self.assertEqual(first_state[-1], result[0])

