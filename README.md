# Stack
Zbiór projektów modelujących jedną z podstawowych struktur danych, czyli stos. Każda implementacja stosu dostarcza podstawowych operacji:

* odłożenia na stos,
* zdjęcia ze stosu,
* pobrania zawartości stosu,
* pobrania rozmiaru stosu.

Projekty związane ze stosem służą nauce testów jednostkowych oraz przypomnieniem podstawowych elementów programistycznych, dla wybranych języków oraz technologii. Projekt jstack może dodatkowo służyć do analizy jakości testów.
## Technologie i projekty
### astack
Angular + TypeScript
### pstack
Python + unittest/pytest
### jstack
Java + Junit 4/5
## Dodatkowe informacje
https://pl.wikipedia.org/wiki/Stos_(informatyka)
